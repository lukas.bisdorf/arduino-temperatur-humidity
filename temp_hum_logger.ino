#include <NTPClient.h>
#include <ESP8266WiFi.h>
#include <ESP8266WiFiMulti.h>
#include <WiFiUdp.h>
#define GMT 0//timezone

#include "DHT.h"
#define DHTTYPE DHT22
#define DHTPIN 2
// Initialize DHT sensor.
DHT dht(DHTPIN, DHTTYPE);

// Temporary variables
static char celsiusTemp[7];
static char humidityTemp[7];
// define timezone offset in seconds
#define UTC 0

WiFiUDP ntpUDP;
ESP8266WiFiMulti WiFiMulti;
NTPClient timeClient(ntpUDP, "ptbtime1.ptb.de", UTC, 3600000);
time_t getNTPtime(void);
WiFiClient client;
WiFiUDP Udp;

IPAddress remoteIP(88,99,149,179); //graphite server
unsigned int remotePort = 2003;  //graphite server port
unsigned int localPort = 2003;

// strings for graphite path
const String location = "a.";
const String baseString = "bisdorf.iot.sensor.";


void setup() {
  // setup wifi on flash

  Serial.begin(9600);
  delay(200);
  pinMode(LED_BUILTIN, OUTPUT);
  // We start by connecting to a WiFi network
  //WiFiMulti.addAP("thewhiterabbit", "foo");
  WiFiMulti.addAP("freifunk-rhein-neckar.de");

  Serial.println();
  Serial.println();
  Serial.print("Wait for WiFi... ");

  while(WiFiMulti.run() != WL_CONNECTED) {
      Serial.print(".");
      delay(500);
  }

  Serial.println("");
  Serial.println("WiFi connected");
  Serial.println("IP address: ");
  Serial.println(WiFi.localIP());

  // sync NTP
  //blinking led if wifi is connected and time is synced
  for (int i=1;i<10;i++){
    digitalWrite(LED_BUILTIN, LOW);
    delay(50);                      
    digitalWrite(LED_BUILTIN, HIGH);
    delay(50); 
  }
  dht.begin();
}

void loop() {
  timeClient.update();
  float temp = 0;
  float hum = 0;
  float pres = 0;
  float vcc = 0;
  int rssi = 0;
  String ssid = "";
  
  temp = dht.readTemperature();
  hum = dht.readHumidity();
  vcc = ESP.getVcc()/1000.0;
  rssi = WiFi.RSSI();
  ssid = WiFi.SSID();
  
  if (isnan(temp) || isnan(hum)) {
    Serial.println("Failed to read from DHT sensor!");

    Serial.println(temp);
    Serial.println(hum);
    strcpy(celsiusTemp,"Failed");
    strcpy(humidityTemp, "Failed");
  }
  else{  
    client.connect(remoteIP, remotePort);
    writeTCP(client, "temp", temp);
    writeTCP(client, "hum", hum);
    //writeTCP(client, "pres", pres);
    writeTCP(client, "vcc", vcc);
    writeTCP(client, "rssi." + ssid, rssi);
    // close tcp connection
    client.stop();
    
  }
  
  /*char graphite_rssi[50];
  int rssi = WiFi.RSSI();
  String ssid = WiFi.SSID();
  String graphite_data = baseString + location + ssid + " ";
  graphite_data.toCharArray(graphite_rssi, 50);
  Udp.beginPacket(remoteIP, remotePort);
  Udp.write(graphite_rssi);
  Udp.print(rssi); 
  Udp.write(" ");
  Udp.println(now());    
  Serial.write(graphite_rssi);
  Serial.print(rssi); 
  Serial.write(" ");
  Serial.println(now());
  Udp.endPacket();*/
  
  delay(55000);
}

void writeTCP(WiFiClient client, String graphiteElement, float value){
    // build line for graphite
    char* finalElement = getCharFromString(baseString + location + graphiteElement + " " + value + " " + timeClient.getEpochTime());
    // print line to serial for debugging
    Serial.println(finalElement);
    // send line over tcp
    client.println(finalElement);
}

// to enable some string concatenations we need this function to get byte arrays again
char* getCharFromString(String input){
    static char output[64] = "";
    input.toCharArray(output, 64);
    return output;
}
